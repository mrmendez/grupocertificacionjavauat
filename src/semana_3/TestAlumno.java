package semana_3;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author mrmendez
 */
public class TestAlumno {
    public static void main(String... args){
        Alumno alumno[] = null;
        List <String>materias = new <String>ArrayList();
        Scanner sc = new Scanner(System.in);
        String nom = null;
        String per = null;
        int c=0;
        System.out.println("¿Cuántos alumnos vas a dar de alta?");
        c = Integer.parseInt(sc.nextLine().trim());
        alumno = new Alumno[c];
        System.out.println("\tSE TE pedirán LOS DATOS DE 2 ALUMNOS\n");
        int matricula = 0;
        
        for (int i=0 ; i < c ; i++){
        System.out.println("DAME UNA LISTA DE MATERIAS PARA UN ALUMNO:");
        System.out.println("Cuando quieras salir teclea \"salir\">");
        int n=1;
        String captura= null;
        System.out.println("Para capturar a un alumno debes capturar primero las materias que lleva.");
        
        while(true){
            System.out.println("Dame el nombre de la materia: "+n+" que lleva el alumno: ");
            captura = sc.nextLine().trim();
            if(!captura.equals("salir"))
            System.out.println("Estas dandole de alta la materia: "+captura.toUpperCase());
            if( captura.equals("salir"))
                break;
            materias.add(captura);
            n++;
        }
        System.out.println("Ahora dame la matrícula del alumno: ");
        matricula = Integer.parseInt(sc.nextLine().trim());
        System.out.println("Ahora dame el nombre del alumno: ");
        nom = sc.nextLine().trim();
        System.out.println("Dame el periodo academico que esta cursando el alumno:");
        per = sc.nextLine().trim();
        alumno[i] = new Alumno(matricula,nom,per,materias);
        System.out.println(alumno[i].toString().toUpperCase());
        materias.clear();
        }
        
        String mensaje= "\nDISTE DE ALTA "+c+" ALUMNOS\n";
        mensaje += "SUS NOMBRE FUERON: \n";
        for(int i=0;i < c;i++){
            mensaje += i+") "+alumno[i].getNombre()+"\n";
        }
        System.out.println(mensaje);
    }
}
