package semana_3;

import java.util.List;

/**
 *
 * @author mrmendez
 */
public class Alumno {

    int matricula;
    String nombre;
    String periodo;
    List materias;

    public Alumno(int mat, String nom, String per, List m) {
        this.matricula = mat;
        this.nombre = nom;
        this.periodo = per;
        this.materias = m;
    }

    // METODOS GETTERS

    public int getMatricula() {
        return this.matricula;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getPeriodo() {
        return this.periodo;
    }

    public List getMaterias() {
        return this.materias;
    }

    //METODOS SETTERS
    public void setMatricula(int n) {
        this.matricula = n;
    }

    public void setNombre(String n) {
        this.nombre = n;
    }

    public void setPeriodo(String n) {
        this.nombre = n;
    }

    public void setMaterias(List n) {
        this.materias = n;
    }

    public String toString() {
        return matricula + " - " + nombre + " - " + periodo + " - " + materias.toString();
    }

    public boolean equals(Object o) {
        Alumno alumno = (Alumno) o;
        return alumno.getMatricula() == this.matricula;
    }

}